from django.apps import AppConfig


class UsersInteractionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'users_interaction'
