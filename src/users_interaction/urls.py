from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from users_interaction.views import main_page

urlpatterns = [
                  path('', main_page, name='main'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
              static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
