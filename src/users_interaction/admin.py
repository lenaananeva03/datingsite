from django.contrib import admin
from django.contrib.auth import get_user_model

from users_interaction.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'avatar', 'email')


admin.register(User, UserAdmin)
