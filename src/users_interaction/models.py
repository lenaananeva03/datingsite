import math

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager as DjangoUserManager
from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    class Meta:
        abstract = True


class UserManager(DjangoUserManager):
    def create_user(self, email, password=None, **kwargs):
        user = self.model(email=email)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password=None, **kwargs):
        user = self.model(email=email, is_superuser=True)
        user.set_password(password)
        user.save()
        return user


class User(BaseModel, AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=20, verbose_name='Имя')
    last_name = models.CharField(max_length=30, verbose_name='Фамилия')
    avatar = models.ImageField(default=None, verbose_name='Фото профиля')
    email = models.EmailField(unique=True, verbose_name='Электронная почта')
    is_women = models.BooleanField(null=True, verbose_name='Это женщина?')
    matched_users = models.ManyToManyField('User')
    longitude = models.FloatField(verbose_name='Долгота')
    latitude = models.FloatField(verbose_name='Широта')

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    USERNAME_FIELD = 'email'

    @property
    def is_staff(self):
        return self.is_superuser

    def get_distance(self, another_user):
        longitude1 = self.longitude
        latitude1 = self.latitude
        longitude2 = another_user.longitude
        latitude2 = another_user.latitude
        arc_length = 6373
        angle = 2 * math.asin(math.sqrt(
            math.pow(math.sin((latitude2 - latitude1) / 2), 2) + math.cos(latitude1) * math.cos(latitude2) *
            math.pow(math.sin((longitude2 - longitude1) / 2), 2)
        ))
        return angle * arc_length
