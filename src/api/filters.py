import django_filters
from django.contrib.auth import get_user_model
from django_filters import rest_framework as filters

from api.services import get_users_by_distance

User = get_user_model()


class UserFilter(filters.FilterSet):
    min_distance = django_filters.NumberFilter(field_name="distance", lookup_expr='gte', label='Минимальная дистанция',
                                               method='get_users_by_min_distance'
                                               )
    max_distance = django_filters.NumberFilter(field_name="distance", lookup_expr='lte', label='Максимальная дистанция',
                                               method='get_users_by_max_distance'
                                               )

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'is_women', 'min_distance', 'max_distance']

    def get_users_by_min_distance(self, queryset, name, value):
        user = self.request.user
        users = User.objects.all()
        max_distance = 12742  # diameter for Earth km
        queryset_result = get_users_by_distance(user, users, value, max_distance)
        queryset = queryset.filter(id__in=queryset_result)
        return queryset

    def get_users_by_max_distance(self, queryset, name, value):
        user = self.request.user
        users = User.objects.all()
        min_distance = 0
        queryset_result = get_users_by_distance(user, users, min_distance, value)
        queryset = queryset.filter(id__in=queryset_result)
        return queryset
