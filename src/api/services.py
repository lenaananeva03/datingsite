import base64
import io
import math
import os
import time
import urllib

from PIL import ImageEnhance
from PIL import Image
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.mail import send_mail
from django.forms import ImageField
from django.shortcuts import get_object_or_404
from django.templatetags.static import static

from datingSite.settings import BASE_DIR

User = get_user_model()


def add_watermark(user):
    watermark_path = get_watermark_path()
    watermark = Image.open(watermark_path)

    file_name = user.avatar.url.split('/')[-1]
    cur_dir = os.path.join(os.path.abspath(BASE_DIR), 'static', 'media')
    avatar_path = os.path.join(cur_dir, file_name)

    avatar = Image.open(avatar_path)
    avatar.paste(watermark, (0, 0))

    os.remove(avatar_path)
    avatar.save(avatar_path, quality=95)

    watermark.close()
    avatar.close()

    user.avatar = avatar_path
    user.save()


def get_watermark_path():
    image_path = os.path.join(os.path.abspath(BASE_DIR), 'api', 'static', 'media', 'watermark.png')
    return image_path


def send_match_mail(to_email, liked_user):
    text = f'Вы понравились {liked_user.first_name}! Почта участника: {liked_user.email}'
    send_mail("Dating Site", text,
              settings.EMAIL_HOST_USER,
              [to_email, ])


def get_users_by_distance(cur_user, users, min_distance, max_distance):
    result = []
    for user in users:
        if user == users:
            continue
        if min_distance <= cur_user.get_distance(user) <= max_distance:
            result.append(user.id)
    return result
