from django.urls import path
from rest_framework.routers import SimpleRouter

from api.views import main_api_view, ClientsViewSet, clients_create_view, ClientsList

router = SimpleRouter()
router.register(r'clients', ClientsViewSet, basename='client')

urlpatterns = [
    path('', main_api_view, name='main'),
    path('clients/create/', clients_create_view, name='clients_create'),
    path('list/', ClientsList.as_view()),
    * router.urls,
]
app_name = 'api'
