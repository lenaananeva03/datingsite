from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from rest_framework import viewsets, generics
from rest_framework.decorators import api_view, action
from rest_framework.response import Response

from api.filters import UserFilter
from api.serializers import RegisterSerializer, UserSerializer
from api.services import send_match_mail

User = get_user_model()


@api_view(['GET'])
def main_api_view(request):
    return Response({'status': 'ok'})


def clients_create_view(request):
    return redirect('client-list')


class ClientsViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer

    @action(detail=True, methods=['get', ])
    def match(self, request, pk=None):
        cur_user = request.user
        matched_user = User.objects.get(id=pk)
        if not (id and matched_user):
            return Response({'status': f'Пользователь с id={pk} не найден'})
        cur_user.matched_users.add(matched_user)
        cur_user.save()
        if cur_user in matched_user.matched_users.all():
            send_match_mail(cur_user.email, matched_user)
            send_match_mail(matched_user.email, cur_user)
            return Response({'status': f'Ваша симпатия взаимна. Вот почта клиента - {matched_user.email}'})
        return Response({'status': 'Симпатия отправлена'})


class ClientsList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filterset_class = UserFilter
