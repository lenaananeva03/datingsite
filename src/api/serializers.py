from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core.files.images import get_image_dimensions
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from api.services import add_watermark

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'is_women', 'avatar', 'longitude', 'latitude')


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password],
                                     style={'input_type': 'password'})
    password2 = serializers.CharField(write_only=True, required=True, style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'is_women', 'avatar', 'longitude', 'latitude', 'password',
                  'password2')

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Пароли не совпадают."})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            is_women=validated_data['is_women'],
            avatar=validated_data['avatar'],
            longitude=validated_data['longitude'],
            latitude=validated_data['latitude']
        )

        user.set_password(validated_data['password'])
        add_watermark(user)
        user.save()

        return user
