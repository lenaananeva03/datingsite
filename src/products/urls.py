from django.urls import path

from products.views import parsing

urlpatterns = [
    path('', parsing, name='main'),
]
