from django.http import HttpResponse

from products.models import Category, Product
from products.services import parsing_products_by_category, save_image


def parsing(request):
    category = Category(title_russian='Щипцы для завивки', title_english='schipcy-dlya-zavivki-volos', children=None)
    category.save()
    products = []
    products = parsing_products_by_category(category, products)
    for product in products:
        product_model = Product(title=product.title, price=product.price, category=category)
        save_image(product_model, product.id)
        product_model.save()
    return HttpResponse("Hello!")
