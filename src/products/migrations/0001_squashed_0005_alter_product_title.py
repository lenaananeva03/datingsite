# Generated by Django 3.2.9 on 2021-11-05 19:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    replaces = [('products', '0001_initial'), ('products', '0002_auto_20211105_2058'), ('products', '0003_auto_20211105_2058'), ('products', '0004_alter_product_price'), ('products', '0005_alter_product_title')]

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('title_russian', models.CharField(max_length=30)),
                ('title_english', models.CharField(max_length=30)),
                ('children', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='parent', to='products.category')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Дата обновления')),
                ('title', models.CharField(max_length=100)),
                ('price', models.CharField(max_length=15)),
                ('image', models.ImageField(upload_to='')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.category')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
