import base64
import os
import re
from urllib.parse import urljoin

import requests
from django.core.files import File

from datingSite.settings import BASE_DIR
from products.entities import Product


def parsing(name_category, result):
    url = urljoin('https://www.citilink.ru/catalog/', name_category)
    text = requests.get(url).text
    data = re.findall('pageData = {.*', text)
    if len(data) != 2:
        print('exception')
        return
    products_data = data[1].split('{')[3:]

    for product in products_data:
        product_split = product.split('","')
        product = Product()
        for field in product_split:
            get_product_by_str(field, product)
        result.append(product)


def get_product_by_str(field, product):
    key_value = field.split('":"')
    if len(key_value) != 2:
        return None
    key = key_value[0]
    value = key_value[1]

    if key.endswith('id'):  # the first field has " at start
        product.id = value
    elif key == 'name':
        product.title = value
    elif key == 'price':
        product.price = value
    elif key == 'category':
        product.category = value
    # return product


def parsing_products_by_category(category, result_products):
    if category.has_related_object():
        for child_category in category.children:
            parsing_products_by_category(child_category, result_products)
    parsing(category.title_english, result_products)
    return result_products


def save_image(product, img_id):
    url = f'https://items.s1.citilink.ru/{img_id}_v01_m.jpg'
    img_name = url.split('/')[-1]
    cur_dir = os.path.join(os.path.abspath(BASE_DIR), 'static', 'media')
    img_path = os.path.join(cur_dir, img_name)

    img_bytes = requests.get(url).content
    with open(img_path, 'wb') as f:
        f.write(img_bytes)
    product.image = img_name
