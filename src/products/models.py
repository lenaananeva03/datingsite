from django.db import models

from users_interaction.models import BaseModel


class Product(BaseModel):
    title = models.CharField(max_length=100)
    price = models.CharField(max_length=15)
    image = models.ImageField()
    category = models.ForeignKey('Category', on_delete=models.CASCADE)


class Category(BaseModel):
    title_russian = models.CharField(max_length=30)
    title_english = models.CharField(max_length=30)
    children = models.ForeignKey('Category', on_delete=models.CASCADE, null=True, related_name='parent')

    def has_related_object(self):
        return hasattr(self, 'customers') and self.children is not None
