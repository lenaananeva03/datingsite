Запуск для локальной разработки


`python -m venv env` - инициализация виртуального окружения

`source env/bin/activate` - вход в виртуальное окружение

`pip install -r requirements.txt` - установка зависимостей

`python src/manage.py migrate` - запуск миграций

`python src/manage.py runserver` - запуск приложения

`python src/manage.py createsuperuser` - создание суперпользователя